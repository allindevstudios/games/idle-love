# The python scripts of the game goes in this file.

init python:
    import time

    class LovePoints(object):
        def __init__(self):
            self.clicked = 0
            self.timed = 0.0

        @property
        def cap(self):
            return 2000 + (upgrades.super_seducer.count ** 3)  * 1000

        @property
        def next_cap_increase(self):
            return ((upgrades.super_seducer.count + 1) ** 3)  * 1000 - (upgrades.super_seducer.count ** 3)  * 1000
        
        @property
        def total(self):
            difference = self.clicked + self.timed - self.cap
            if difference > 0:
                self.deduce(difference)
            return min(int(self.clicked + self.timed), self.cap)

        @property
        def total_bar(self):
            difference = self.clicked + self.timed - self.cap
            if difference > 0:
                self.deduce(difference)
            return int(float(min(int(self.clicked + self.timed), self.cap)) / self.cap * 2000)

        @total_bar.setter
        def total_bar(self, value):
            return

        def deduce(self, amount):
            clicking_reduction = min(self.clicked, amount)
            self.clicked -= clicking_reduction
            amount -= clicking_reduction
            timed_reduction = min(self.timed, amount)
            self.timed -= timed_reduction
            
    class PointsPerSeconds(object):
        def __init__(self):
            self.previous = time.time()
            self.raw = 0.0
            

        def points_since(self, previous_time):
            now = time.time()
            return (now - previous_time) * self.total

        @property
        def total(self):
            return (self.raw + 0.5 * upgrades.bots.count + 100 * upgrades.tickets.count) * (upgrades.pheromones.count + 1)

    class Upgrade(object):
        def __init__(self, name, initial_price):
            self.name = name
            self.initial_price = initial_price
            self.count = 0

        @property
        def cost(self):
            return int(self.initial_price * ((1 + float(self.count)/12.0) ** 2.5))

        def add(self):
            global love_points
            if love_points.total >= self.cost:
                love_points.deduce(self.cost)
                self.count += 1
                renpy.restart_interaction()

    def can_pay(cost):
        global love_points
        if love_points.total >= cost:
            love_points.deduce(cost)
            return True
        return False
    

    class Pheromones(Upgrade):
        def __init__(self):
            super(Pheromones, self).__init__("pheromones", 250)

        @property
        def cost(self):
            return int(self.initial_price * ((1 + float(self.count)/5.0) ** 3))

    class SuperSeducer(Upgrade):
        def __init__(self):
            super(SuperSeducer, self).__init__("super-seducer", 2000)

        @property
        def cost(self):
            return love_points.cap

    class Upgrades(object):
        def __init__(self):
            self.bots = Upgrade("bots", 10)
            self.super_seducer = SuperSeducer()
            self.pheromones = Pheromones()
            self.tickets = Upgrade("tickets", 10000)

    import pygame
    import math

    class LovePointCounter(renpy.Displayable):
        def __init__(self, **kwargs):
            super(LovePointCounter, self).__init__(**kwargs)

        def render(self, width, height, st, at):
            r = renpy.Render(width, height)

            global points_per_second
            now = time.time()
            if now - points_per_second.previous > 0.33:
                global love_points
                global points_per_second
                love_points.timed += points_per_second.points_since(points_per_second.previous)
                points_per_second.previous = time.time()
                renpy.restart_interaction()

            renpy.redraw(self, 0)

            return r

        
    def button_pressed():
        global love_points
        love_points.clicked += 1 

    def use_love(cost):
        global love_points
        global base_index
        global total_cost
        if love_points.total >= cost:
            love_points.deduce(cost)
            base_index += 1
            total_cost = base_cost**base_index
            return True
        return False

    def add_upgrade(upgrade_name):
        global upgrades
        upgrades.upgrade(upgrade_name)

    ButtonPressed = renpy.curry(button_pressed)
    AddUpgrade = renpy.curry(add_upgrade)


define previous = 0.0

define love_points = LovePoints()
define points_per_second = PointsPerSeconds()
define upgrades = Upgrades()


screen show_love():
    add LovePointCounter():
        xalign 0.5
        yalign 0.5

# screen show_love_retry():
#     add LovePointCounter():
#         xalign 0.5
#         yalign 0.5

#     add love_button():
#         xalign 0.5
#         yalign 0.5

# screen show_interface():
#     use show_love()
#     use show_love_retry()

label love_improvements:

    hide screen say

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.
    show screen love_button
    show screen show_love

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.

    # These display lines of dialogue.

    e "You've created a new Ren'Py game."

    e "Once you add a story, pictures, and music, you can release it to the world!"

    e "You have [love_points] love points!!!"

    # This ends the game.

    return
