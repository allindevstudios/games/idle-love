﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define u = Character("???", who_color="#FFC0CB")
define a = Character("Alina", who_color="#FFC0CB")
define l = Character("Lady Arianne LeFay", who_color="#4169e1")
define b = Character("Giantess Birsha", who_color="#FF4500")
define d = Character("Demon Queen Caia", who_color="#8b0000")

define o1 = Character("Fillibus, Gelatinous Blob", who_color="#49B960")
define o2 = Character("Gomorron, Emerald Dragon Hybrid", who_color="#50c878")
define o3 = Character("Hela, Imp", who_color="#003366")

define base_cost = 10
define base_index = 3
define total_cost = base_cost**base_index
define current_label = "no_love"

define doors_used = 0
define hell_door_used = False
define blob_door_used = False
define dragon_door_used = False
define giant_door_used = False

# The game starts here.

screen item_tooltip(tooltip_text):
    $ x,y = renpy.get_mouse_pos()
    frame:
        pos (x, y) anchor (0.5, 1.0)
        vbox:
            text tooltip_text size 12 xsize 256 justify False

screen item_amount(item_sprite, item_object):
    $ item_sprite += "_%s.png"
    hbox:
        spacing 10
        imagebutton auto item_sprite
        text "x [item_object.count]" yalign 0.5


screen shop_item(item_label , item_object, item_sprite, tooltip_text):
    $ item_sprite += "_%s.png"

    vbox:
        spacing 10
        text item_label text_align 0.5
        frame at center:
            imagebutton auto item_sprite:
                hovered Show("item_tooltip", None, tooltip_text)
                unhovered Hide("item_tooltip")
                action item_object.add

        textbutton "Buy ([item_object.cost] {image=heart-small.png})" at center:
            action item_object.add

screen upgrades_overview():
    vbox:
        spacing 10
        xalign 0.2
        yalign 0.3
        use item_amount("instagram", upgrades.bots)
        use item_amount("pheromones", upgrades.pheromones)
    
    vbox:
        spacing 10
        xalign 0.8
        yalign 0.3
        use item_amount("super-seducer-2", upgrades.super_seducer)
        use item_amount("mcr", upgrades.tickets)

screen love_shop():
    #modal True
    $ item_labels = ["Instagram Bots\n", "α-factor Mating\nPheromones","Super Seducer 2\nSteam Keys",  "My Chemical Romance\nConcert Tickets"]
    $ item_sprites = ["instagram", "pheromones",  "super-seducer-2", "mcr"]
    $ item_objects = [upgrades.bots, upgrades.pheromones, upgrades.super_seducer, upgrades.tickets]
    $ item_tooltips = ["An army of bots spreads your message of love and care throughout the vilest reaches of the internet, increasing your love generation as a result!\n(+0.5 LPS)"]
    $ item_tooltips.append("Five centuries of research and extensive testing on humans have led to a revolutionary breakthrough formula that increases your attractiveness manifold.\n(multiplies Love Speed with amount of pheromones + 1)")
    $ item_tooltips.append("A Copy of this amazing cult classic fills your Steam library with the exact tips and tricks to charm and conquer even the most frigid of ice queens.\n(+[love_points.next_cap_increase] Love capacity)")
    $ item_tooltips.append("Inviting a lady to this amazing band gives them the opportunity to fall even deeper in love with your good looks.\n(+100 LPS)")

    frame:
        xpadding 10
        ypadding 10
        xalign 0.5
        yalign 0.9

        vbox:
            text "~ Love Shop <3 ~" at center
            null height 10
            grid 4 1:
                xspacing -64
                for item_index in range (4):
                    use shop_item(item_labels[item_index], item_objects[item_index], item_sprites[item_index], item_tooltips[item_index])

screen waifu_button():
    textbutton "Start charming" xalign 0.5 yalign 0.5

screen love_bar(frame_background = None):
    frame:
        background frame_background
        bar:
            value FieldValue(love_points, "total_bar", love_points.cap)
            xmaximum 1024
            xalign 0.5
            yoffset 32
        text "[love_points.total] / [love_points.cap] ([points_per_second.total] LPS)":
            xalign 0.5
            yoffset 32

screen swap_to_waifu_screen():
    imagebutton auto "waifu-arrow_%s":
        xalign 1.0
        yalign 0.5 
        action Return(True)

screen swap_to_love_screen():
    imagebutton auto "love-arrow_%s":
        xalign 0.0
        yalign 0.5 
        action Jump("show_love_screen")

screen love_button():
    imagebutton auto "large-heart_%s":
        xalign 0.5
        yalign 0.25

    imagebutton auto "heart_%s":
        xalign 0.5
        yalign 0.3
        action [
            ButtonPressed(),
            renpy.restart_interaction
        ]
    imagebutton auto "touch-me_%s":
        xalign 0.5
        yalign 0.33

screen love_screen():
    use show_love()
    use love_bar(Solid("#000000"))
    use upgrades_overview()
    use love_shop()
    use love_button()
    use swap_to_waifu_screen()

screen waifu_screen():
    use love_bar(None)
    use swap_to_love_screen()

screen ending_screen():
    vbox:
        xalign 0.5
        yalign 0.5
        frame:
            xalign 0.5
            yalign 0.5
            xpadding 100
            ypadding 100
            text "Thanks for playing and (hopefully) enjoying this monstrosity!\n\nLaurent Van Acker & Piet Bronders":
                text_align 0.5

        textbutton "Back to Menu":
            xalign 0.5
            yalign 0.5
            action [
                Return(True)
            ]

# Start the game!
label start:
    jump show_waifu_screen

label show_love_screen:
    window hide
    call screen love_screen

    if _return == True:
        jump show_waifu_screen

label show_waifu_screen:
    window show
    show screen show_love
    show screen waifu_screen

    $ renpy.jump(current_label)

label no_love:

    "Ugh..."

    "Where am I?"

    "..."

    "You try to open your eyes and move your limbs, but a great fatigue takes hold of your entire being."

    u "Hello? are you ok?"

    "Again, you try to open your eyes and... succeed.\nYou are greeted by a peasant girl leaning over you."

    show bg farm with dissolve

    a "Sir? What is your affiliation?\nWhat are you doing here in my pigpen?"

    "You try to remember what happened the night before..."

    "Out of nowhere a horrible headache takes hold of your entire being and you remember what happened..."

    "A sick university party and a bottle of Everclear!"

    "Your throat is as dry as a bone! You try to use your voice and a rasping screeching comes out:\n{i}\'water...please...\'{/i}"

    a "Sir, you stink of booze and have been rolling in manure for the better part of the day. I {b}demand{/b} an explanation!"

label before_first_love:
    $ current_label = "before_first_love"
    menu:
        "Unable to even respond, you attempt to channel your manly charms to get her to at least give you some water."

        "Charm her while lying in a puddle of your own vomit. ([total_cost] {image=heart-small.png})":
            if use_love(total_cost):
                a "..."
            else:
                $ renpy.jump(current_label)

label after_first_love:
    $ current_label = "after_first_love"

    a "You poor man... you are clearly sick...\nI shouldn't have been shouting at you..."

    "Alina helps you up and half-carries you to a sodden medieval-looking farmhouse."

    show bg house with dissolve

    "You enter the house and are flabbergasted by the fact that there's no WiFi indicator appearing on your smartphone's display."

    a "Sir? Would you like some refreshments? I'm afraid we don't have any more clean water in the tub..."

label refreshement_menu:
    menu:
        "What should I ask her first?"

        "Could you give me the WiFi password, please?":
            a "WiFi? Sir, I don't know any passwords... I'm but a simple peasant whench."

            "What? How in god's name has Alina been surviving here without any of that online validation you crave for on a daily basis?"

            a "I can see you are dissappointed, Sir... I am sorry for not meeting your expectations."

            jump refreshement_menu

        "How about a large jar of crystalclear water?":
            a "Right up, Sir!"

            "You gulp down the entire jar in one go and already start to feel moderately better."

            jump refreshement_menu

        "I've had enough refreshments! Time to head up out into the unknown!":
            pass

    "Out of nowhere, a brigade of funky looking men bursts into Alina's shack."

    "Leading them is a lady in full plate armor showcasing a permantent sneer on her otherwise pretty face."

    l "Peasants! I've come to collect the one summoned by the arch wizard Horvath! He is to disembark on a mission to defeat the demon queen at once!"

    l "Unfortunately the wizard had one too many sniffs of glue and messed up the teleportation coordinates."

    l "Do any of you peasants know of such an event?"

    "You clearly state that it is you, the university bachelor, that has been wrongfully summoned and that you should most definitely be resting in a bed right now after that sick rave."

    l "YOU? A hero from another world? Puh! You are just a stinking peasant! Look at your attire!"

label before_second_love:
    $ current_label = "before_second_love"

    menu:
        "This highborn has no idea of the man she is messing with, time to show her the extent of your persuasiveness."

        "Charm her in an attempt to prove that you are indeed the summoned hero. ([total_cost] {image=heart-small.png})":
            if use_love(total_cost):
                "You show her your smartphone and other modern trinkets that will surely convince her of your origins."
                l "What is this thing? A \'smartphone\' you say?"
                l "Such a device is clearly demonic in nature! Guards beat the everliving snot out of this devil worshipper!"
            else:
                $ renpy.jump(current_label)

label after_second_love:
    $ current_label = "after_second_love"

    "You start running your ass out of there as you have no intention of becoming a punching bags for any of these men."

    l "SEIZE HIM!"

    "After a long and daring parcours challenge throughout the entire village, you finally succeed in throwing the guards off long enough to escape into the woods."

    show bg woods with dissolve

    "You lean against a tree and catch up your breath..."

    "A blood-hurdling scream resonates through your entire being, making you wet your pants and and shit them as well in the pure horror of the moment."

    "You sneak over a ridge and see that the entire demon army has been secretively preparing their attack on the kingdom in these pleasant woods!"

    "Out of seemingly nowhere, a huge hand graps your collar and drags you up several meters from the ground!"

label password_guessing:
    menu:
        b "WHAT IS DEMON PASSWORD?"

        "\"Hail to the demon queen?\"":
            b "IS CORRECT PASSWORD!"

            "A winged batperson lands on her shoulder and whispers something in her ear."

            b "NO, IS WRONG PASSWORD!"

            jump password_guessing

        "\"human flesh for all?\"":
            b "IS ALMOST WRONG PASSWORD!"

            jump password_guessing

        "\"Please, let me down?\"":
            b "IS TOTALLY WRONG PASSWORD!"

            "The giantess seems to ponder for but a brief moment..."

            b "AND ANSWER TO QUESTION IS \"NO!\""

            jump password_guessing

        "I'm just a lowly demon, I don't know stuff like that!":
            b "IS WRONG PASSWORD!"

            b "YOU NO PART OF ARMY..."

            "The giantess\' stomach starts to rumble and she move her hand over her belly."

            b "YOU LOOK TASTY! I HUNGRY!"

label before_third_love:
    $ current_label = "before_third_love"

    menu:
        "Her gaping maw comes closer and closer...time seems to slowly come to a halt. Being devoured by a giantess wasn't part of your bucketlist!"

        "Charm her while having thouroughly soaked your pants. ([total_cost] {image=heart-small.png})":
            if use_love(total_cost):
                b "YOU STINK OF PEE, VOMIT AND DROPPING!"

                b "..."

                b "APPETITE GONE..."
            else:
                $ renpy.jump(current_label)

label after_third_love:
    $ current_label = "after_third_love"

    b "YOU GO WASH IN DEMON BATHHOUSE!"

    b "I FIND WHEN HUNGRY! BETTER CLEAN SOON OR I PULL LEG OFF!"

    "The giantess strolls off and looks digustingly at her know pee-soaked hand."

    show bg army camp with dissolve

    "You limp thoughout the entire demon camp in an attempt to find the bathhouse. After asking and convincing some dour-looking elves of your bad intentions you finally find yourself infront of the demon spa."

    "Inside, you are faced with four doors with depictions of demonic glyphs that seem to indicate some kind of divider between the species inhabiting the demon camp."

    show bg bathhouse with dissolve

label choose_correct_door:
    menu:
        "Better choose the correct door or you'll end up as minced meat!"
        
        "{image=symbols1.png}" if doors_used < 4:
            o1 "Excuse me... You aren't supposed to be in this section..."

            "You profusely apologize and are quite embarassed about the whole ordeal."

            o1 "Don't worry, taking the wrong door can happen to anyone! Just take the door with the humanoid glyph and you'll be fine!"

            if blob_door_used == False:
                $ blob_door_used = True
                $ doors_used += 1
            jump choose_correct_door

        "{image=symbols2.png}" if doors_used < 4:
            "Nervously you open the door and are greeted by...."

            "GIANTESS BIRSHA!!!"

            b "YOU NO CLEAN! YOU TOOK WRONG DOOR!"

            b "GO BACK! TAKE DOOR WITH \'HUMANOID\' SYMBOL!"

            "Birsha again grabs you by the collar and throws you out."

            if giant_door_used == False:
                $ giant_door_used = True
                $ doors_used += 1
            jump choose_correct_door

        "{image=symbols3.png}" if doors_used < 4:
            "You are greeting by a fierce green-scaled dragon roaring golden flames across the entire ceiling."

            o2 "RAWR! Gomorron is a very naughty dragon!"

            "You slowly back away..."

            if dragon_door_used == False:
                $ dragon_door_used = True
                $ doors_used += 1
            jump choose_correct_door

        "{image=symbols4.png}" if doors_used < 4:
            o3 "Goddamnit! Can't I use the fricking bath without some bumbling buffoon disturbing me every single second?!"

            o3 "I will take my complaints to the owner of this establishment! This has gone too far!"

            "The imp angrily leaves the room while pushing you out."

            o3 "HUMANOIDS ARE ONLY PERMITTED IN THE {b}HUMANOID{/b} SECTION!"

            if hell_door_used == False:
                $ hell_door_used = True
                $ doors_used += 1
            jump choose_correct_door

        "Open the secret hatch hidden beneath the foyer's carpet" if doors_used > 3:

            "After noticing a bump in the carpet, you find a secret hatch... where would this lead to?"
            pass

    show bg onsen with dissolve

    "After exploring the underground passage you exit into an outdoors plazza with a large thermally-heated pool. A young woman is bathing and fully relaxing in the water."

    "You don\'t want to be mistaken for a pervert and thus attempt to sneakily sneak away."

    d "Who is there? Who are you? And what are you doing in my private quarters?"

    d "Explain yourself, human, lest I channel my demonic powers to turn your guts inside-out!"

label before_fourth_love:
    $ current_label = "before_fourth_love"

    menu:
        "Your destiny beckons! Time to put an stop to all future demon-human conflicts!"

        "Charm the demon queen and finally end all wars. ([total_cost] {image=heart-small.png})":
            if use_love(total_cost):
                d "...?"
            else:
                $ renpy.jump(current_label)

label after_fourth_love:
    $ current_label = "after_fourth_love"

    d "What a feeble attempt at charming me! Do you take me for some back-alley prostitute?"

    d "I've heard enough of your disgusting groveling! Prepare to feel the full wrath of my demonic magic!"

    "The demon queen start channeling her demonic magicks and... "

    "A purple fluorescent lighning bolt strikes your pants and makes it catch on fire. You scream like a little bitch as your flesh melts of your nether jewels."

    d "Now scram! Before I do some real damage to something that's actually worth a damn!"

    "You scurry away, trip on a suspiciously-looking bar of soap and break your neck."

label love_ending:
    $ current_label = "love_ending"
    window hide
    call screen ending_screen
    if _return == True:
        return