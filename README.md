# idle-love

Idle Love is a small game made to try out the RenPy Visual Novel Engine and is an attempt to combine both Visual Novel and Idle Clickers genres in an interesting way.

**IMPORTANT**: Game is playable in the browser but runs much smoother and is more reliable when a non-web version is downloaded and played.

## Controls

Simply touch the heart and generate some love, ya'll! After generating some love you can buy upgrades and/or progress the story.

## Credits

**Piet Bronders** - Programming & Story

**Laurent Van Acker** - Programming & Art

Made with [RenPy Visual Novel Engine](https://www.renpy.org/) v7.3.5 & Asesprite